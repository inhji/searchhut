//go:build generate
// +build generate

package query

import (
	_ "git.sr.ht/~emersion/gqlclient/cmd/gqlclientgen"
)

//go:generate go run git.sr.ht/~emersion/gqlclient/cmd/gqlclientgen -s ../graph/schema.graphqls -q queries.gql -o gql.go
