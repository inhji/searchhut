package config

import (
	"log"

	"github.com/vaughan0/go-ini"
)

var ConfigPaths []string = []string{
	"./config.ini",
	"/etc/searchhut/config.ini",
}

func Load() *ini.File {
	for _, path := range ConfigPaths {
		if file, err := ini.LoadFile(path); err == nil {
			return &file
		}
	}
	log.Fatal("Unable to open config file")
	return nil
}
