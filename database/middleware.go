package database

import (
	"database/sql"
	"net/http"
)

var dbCtxKey = &contextKey{"database"}

type contextKey struct {
	name string
}

func Middleware(db *sql.DB) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := Context(r.Context(), db)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}
