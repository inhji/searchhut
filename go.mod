module git.sr.ht/~sircmpwn/searchhut

go 1.18

require (
	git.sr.ht/~emersion/gqlclient v0.0.0-20220202181617-4e6e9c763dd2
	git.sr.ht/~sircmpwn/getopt v1.0.0
	github.com/99designs/gqlgen v0.17.12
	github.com/dustin/go-humanize v1.0.0
	github.com/go-chi/chi v1.5.4
	github.com/go-shiori/go-readability v0.0.0-20220215145315-dd6828d2f09b
	github.com/lib/pq v1.10.6
	github.com/mattn/go-runewidth v0.0.13
	github.com/prometheus/client_golang v1.12.2
	github.com/temoto/robotstxt v1.1.2
	github.com/vaughan0/go-ini v0.0.0-20130923145212-a98ad7ee00ec
	github.com/vektah/gqlparser/v2 v2.4.6
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f
)

require (
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/dave/jennifer v1.4.1 // indirect
	github.com/go-shiori/dom v0.0.0-20210627111528-4e4722cd0d65 // indirect
	github.com/gogs/chardet v0.0.0-20191104214054-4b6791f73a28 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/matryer/moq v0.2.7 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.32.1 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/urfave/cli/v2 v2.8.1 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220106191415-9b9b3d81d5e3 // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.10 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
