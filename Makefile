.POSIX:
.SUFFIXES:

all: sh-admin sh-api sh-index sh-search sh-web

generate:
	go generate ./...

sh-admin: generate
	go build -o sh-admin cmd/sh-admin/main.go

sh-api: generate
	go build -o sh-api cmd/sh-api/main.go

sh-index: generate
	go build -o sh-index cmd/sh-index/main.go

sh-search: generate
	go build -o sh-search cmd/sh-search/main.go

sh-web: generate
	go build -o sh-web cmd/sh-web/main.go

.PHONY: all sh-admin sh-api sh-index sh-search sh-web generate
