package main

import (
	"database/sql"
	"log"
	"os"
	"strings"

	_ "github.com/lib/pq"

	"git.sr.ht/~sircmpwn/searchhut/config"
)

func main() {
	query := strings.Join(os.Args[1:], " ")

	conf := config.Load()
	connstr, ok := conf.Get("searchhut", "connection-string")
	if !ok {
		log.Fatal("Configuration missing connection string")
	}

	db, err := sql.Open("postgres", connstr)
	if err != nil {
		log.Fatal(err)
	}

	// TODO: Avoid parsing the query vector twice
	rows, err := db.Query(`
		SELECT
			title,
			url,
			ts_rank_cd(fts_vector, websearch_to_tsquery('english', $1), 32) AS rank
		FROM page
		WHERE
			title IS NOT NULL AND
			websearch_to_tsquery('english', $1) @@ fts_vector
		ORDER BY rank DESC
		LIMIT 10;
	`, query)
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var (
			title string
			url   string
			rank  float64
		)
		if err := rows.Scan(&title, &url, &rank); err != nil {
			log.Fatal(err)
		}
		log.Printf("%s: %s", url, title)
	}
}
