package main

import (
	"html/template"
	"log"
	"net/http"
	"time"

	"git.sr.ht/~emersion/gqlclient"
	"github.com/dustin/go-humanize"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"git.sr.ht/~sircmpwn/searchhut/config"
	"git.sr.ht/~sircmpwn/searchhut/query"
)

type IndexPage struct {
	Page  string
	Title string
	Query string
	Npage string
}

type SearchPage struct {
	Page    string
	Title   string
	Query   string
	Time    string
	Results []query.Result
	Npage   string // Unused
}

type AboutPage struct {
	Page      string
	Title     string
	CrawlerUA string
}

type router struct {
	*chi.Mux
}

func (r *router) Get(pattern string, handlerFn http.HandlerFunc) {
	r.Mux.Get(pattern, handlerFn)
	r.Mux.Head(pattern, handlerFn)
}

func main() {
	conf := config.Load()
	port, ok := conf.Get("searchhut::web", "bind")
	if !ok {
		port = ":8081"
	}

	router := router{chi.NewRouter()}
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Timeout(10 * time.Second))

	// TODO: Let the user install these somewhere else
	tmpl, err := template.ParseGlob("templates/*.html")
	if err != nil {
		panic(err)
	}
	fs := http.FileServer(http.Dir("./static"))
	router.Handle("/static/*", http.StripPrefix("/static/", fs))

	// TODO: Fetch me from config
	client := gqlclient.New("http://localhost:8080/query", nil)

	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		results, err := query.GetStats(client, r.Context())
		if err != nil {
			log.Println(err)
			http.Error(w, "Error loading index", http.StatusInternalServerError)
			return
		}
		err = tmpl.ExecuteTemplate(w, "index.html", &IndexPage{
			Page:  "index",
			Title: "searchhut",
			Npage: humanize.Comma(int64(results.Npages)),
		})
		if err != nil {
			log.Println(err)
			http.Error(w, "Error displaying page", http.StatusInternalServerError)
			return
		}
	})

	router.Get("/search", func(w http.ResponseWriter, r *http.Request) {
		q := r.FormValue("q")
		if q == "" {
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		start := time.Now()
		results, err := query.Search(client, r.Context(), q)
		if err != nil {
			log.Println(err)
			http.Error(w, "Error performing search query", http.StatusInternalServerError)
			return
		}
		diff := time.Now().Sub(start)
		err = tmpl.ExecuteTemplate(w, "search.html", &SearchPage{
			Page:    "search",
			Title:   q + ": searchhut",
			Query:   q,
			Time:    diff.String(),
			Results: results,
		})
		if err != nil {
			log.Println(err)
			http.Error(w, "Error displaying page", http.StatusInternalServerError)
			return
		}
	})

	router.Get("/about", func(w http.ResponseWriter, r *http.Request) {
		ua, ok := conf.Get("searchhut", "user-agent")
		if !ok {
			ua = ""
		}
		err := tmpl.ExecuteTemplate(w, "about.html", &AboutPage{
			Title:     "About searchhut",
			Page:      "about",
			CrawlerUA: ua,
		})
		if err != nil {
			log.Println(err)
			http.Error(w, "Error displaying page", http.StatusInternalServerError)
			return
		}
	})

	log.Printf("Running server on %s", port)
	log.Fatal(http.ListenAndServe(port, router))
}
