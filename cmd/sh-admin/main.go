package main

import (
	"database/sql"
	"log"
	"os"

	"git.sr.ht/~sircmpwn/getopt"
	"github.com/lib/pq"

	"git.sr.ht/~sircmpwn/searchhut/config"
)

func main() {
	var (
		authoritative bool
		tags          []string
	)
	opts, optind, err := getopt.Getopts(os.Args, "at:")
	if err != nil {
		panic(err)
	}
	for _, opt := range opts {
		switch opt.Option {
		case 'a':
			authoritative = true
		case 't':
			tags = append(tags, opt.Value)
		}
	}
	args := os.Args[optind:]
	domain := args[0]

	conf := config.Load()
	connstr, ok := conf.Get("searchhut", "connection-string")
	if !ok {
		log.Fatal("Configuration missing connection string")
	}

	db, err := sql.Open("postgres", connstr)
	if err != nil {
		log.Fatal(err)
	}

	var id int
	row := db.QueryRow(`
		INSERT INTO domain (
			hostname,
			authoritative,
			tags
		) VALUES ($1, $2, coalesce($3, '{}'::text[])) RETURNING id;
	`, domain, authoritative, pq.Array(tags))
	if err := row.Scan(&id); err != nil {
		log.Fatal(err)
	}
	log.Printf("Added domain %s with ID %d", domain, id)
}
