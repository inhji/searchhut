package model

import (
	"time"
)

type Page struct {
	ID          int       `json:"id"`
	URL         string    `json:"url"`
	LastIndexed time.Time `json:"last_indexed"`
	Title       *string   `json:"title"`
	Language    *string   `json:"language"`
	Description *string   `json:"description"`
	Author      *string   `json:"author"`
	Excerpt     *string   `json:"excerpt"`

	DomainID int
}
